# Installation

Install remotely by running

```shell
bash <(curl -s https://gitlab.com/sunnhas/dotfiles/raw/master/setup-remote.sh)
```

or clone the repository and run the setup script manually

```shell
git clone https://sunnhas@gitlab.com/sunnhas/dotfiles.git ~/.dotfiles
cd ~/.dotfiles && . ./setup.sh
```


# Resources

This repository has been inspired by the following projects:

[Nick S. Plekhanov - dotfiles](https://github.com/nicksp/dotfiles)

[Lars Kappert - dotfiles](https://github.com/webpro/dotfiles)

[Mathias Bynens - dotfiles](https://github.com/mathiasbynens/dotfiles)

[Dries Vints - dotfiles](https://github.com/driesvints/dotfiles)


# License

This project is available under the [MIT License](LICENSE)
