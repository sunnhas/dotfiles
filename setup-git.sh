#!/usr/bin/env bash

echo "Setting up the Git repo so you can just update with git pull"
git clone git@gitlab.com:sunnhas/dotfiles.git ./.tmp

cp -r .tmp/.git .git
rm -rf .tmp

git reset --hard

echo "All done! To update run 'cd ~/.dotfiles && git pull'"
