DOTFILES=$HOME/.dotfiles

export ZSH=$HOME/.oh-my-zsh
export ZSH_CUSTOM=$DOTFILES/zsh

# ZSH_THEME="robbyrussell"
ZSH_THEME="nick"

plugins=(git)

source $DOTFILES/shell/paths

### Handle alias ###
for file in $(find ${DOTFILES}/shell -name functions); do
  source ${file}
done;

### Handle alias ###
for file in $(find ${DOTFILES}/shell -name alias); do
  source ${file}
done;

# Homebrew
export HOMEBREW_NO_ANALYTICS=1 # Don't send analytics https://docs.brew.sh/Analytics.html
alias cask='brew cask'

# Cli
alias c="clear"
alias cli.refresh="source ~/.zshrc"


# Source local extra (private) settings specific to machine if it exists
[ -f ~/.zsh.local ] && source ~/.zsh.local

source $ZSH/oh-my-zsh.sh
