#!/usr/bin/env bash

[[ -x `command -v wget` ]] && CMD="wget --no-check-certificate -O -"
[[ -x `command -v curl` ]] >/dev/null 2>&1 && CMD="curl -#L"

if [ -z "$CMD" ]; then
  echo "No curl or wget available. Aborting."
else
  echo "Downloading dotfiles..."
  mkdir -p ~/.dotfiles

  eval "$CMD https://gitlab.com/sunnhas/dotfiles/repository/archive.tar.gz?ref=master | tar -xzv -C ~/.dotfiles --strip-components=1 --exclude='{.gitignore}'"
  
  cd ~/.dotfiles
  . ./setup.sh

  . ./setup-git.sh
fi
