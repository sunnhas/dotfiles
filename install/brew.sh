#!/usr/bin/env bash

# Homebrew
hash brew 2>/dev/null;
if [ $? -eq 1 ]; then
  echo "Homebrew is not installed. I'll do that right away."
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
fi

brew update
brew upgrade


# CLI Utilities
brew install coreutils
# brew install moreutils
# brew install findutils
brew install tree
brew install jq
brew install speedtest-cli
brew install pwgen
brew install gnupg # gpg
brew install wifi-password

# Development tools
brew install git
brew install node

# DevOps tools
brew install terraform
# brew install ansible
# brew install sshpass

# Gradle tools
brew tap gdubw/gng
brew install gng

# Trivy scan
brew tap aquasecurity/trivy
brew install aquasecurity/trivy/trivy


# Caskroom
brew tap homebrew/cask-drivers # To install Sonos

apps=(
  # Browsers
  vivaldi
  # google-chrome
  # firefox

  # Code editors
  intellij-idea
  visual-studio-code

  # Development tools
  docker
  iterm2
  tower
  postman
  dbeaver-community
  aws-vpn-client
  lens

  # Design tools
  figma

  # Utility tools
  1password
  keeper-password-manager
  bettertouchtool
  alfred

  # Other tools
  slack
  spotify
  sonos
  messenger
)

brew install --cask "${apps[@]}"

# Fonts
# brew tap homebrew/cask-fonts
# brew cask install font-monoid


# Mac App Store CLI
# brew install mas

# mas install 497799835 # Xcode
# mas install 1176895641 # Spark
# mas install 967805235 # Paste
# mas install 411643860 # DaisyDisk
# mas install 409201541 # Pages
# mas install 409203825 # Numbers
# mas install 409183694 # Keynote

# Cleanup
brew cleanup
