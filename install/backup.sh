#!/usr/bin/env bash

# Backup current dotfiles
echo "I'll just backup your current dotfiles - just in case"

date=$(date '+%Y-%m-%dT%H:%M:%S')
backupDir="$HOME/.dotfiles-backup/$date"
backupFiles=(
  '.gitconfig'
  '.gitignore_global'
  '.zshrc'
)

mkdir ${backupDir}

for file in "${backupFiles[@]}"; do
  if [ -d "${file}" ]; then
    ## Handle directory
    mv $HOME/$file $backupDir/$file
  elif [ -f "${file}" ]; then
    mv $HOME/$file $backupDir/$file
  fi
done;

echo "Backup done: ${backupDir}"
