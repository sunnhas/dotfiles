#!/usr/bin/env bash

[[ -e utils/utils.sh ]] || { echo >&2 "Please cd into the bundle before running this script."; exit 1; }

source utils/utils.sh

echo "Installing dotfiles..."

###############################################################################
# Backup                                                                      #
###############################################################################

ask "Should i do a backup?" Y && source install/backup.sh


###############################################################################
# XCode Command Line Tools                                                    #
###############################################################################

notInstalled="xcode-select: note: install requested for command line developer tools"

if [[ $((xcode-\select --install) 2>&1) == ${notInstalled} ]]; then
#if [ ! xcode-select --print-path &> /dev/null ]; then

  # Prompt user to install the XCode Command Line Tools
  xcode-select --install &> /dev/null

  # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  # Wait until the XCode Command Line Tools are installed
  echo -n "Waiting"
  while [[ $((xcode-\select --install) 2>&1) == ${notInstalled} ]]; do
    echo -n "."
    sleep 5
  done
  #until xcode-select --print-path &> /dev/null; do
    #sleep 5
  #done

  print_result $? 'Install XCode Command Line Tools'

  # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  # Point the `xcode-select` developer directory to
  # the appropriate directory from within `Xcode.app`
  # https://github.com/alrra/dotfiles/issues/13

  # sudo xcode-select -switch /Applications/Xcode.app/Contents/Developer
  # print_result $? 'Make "xcode-select" developer directory point to Xcode'

  # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  # Prompt user to agree to the terms of the Xcode license
  # https://github.com/alrra/dotfiles/issues/10

  # sudo xcodebuild -license
  # print_result $? 'Agree with the XCode Command Line Tools licence'

fi


###############################################################################
# Homebrew                                                                    #
###############################################################################

ask "Install brew apps?" Y && source install/brew.sh


###############################################################################
# macOS Settings                                                              #
###############################################################################

ask "Install macOS settings?" N && source install/macos.sh


###############################################################################
# Oh my zsh                                                                   #
###############################################################################

install_zsh () {
  # Test to see if zshell is installed.  If it is:
  if [ -f /bin/zsh -o -f /usr/bin/zsh ]; then
    # Install Oh My Zsh if it isn't already present
    if [[ ! -d $dir/oh-my-zsh/ ]]; then
      sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
    fi
    # Set the default shell to zsh if it isn't currently set to zsh
    if [[ ! $(echo $SHELL) == $(which zsh) ]]; then
      chsh -s $(which zsh)
    fi
  else
    # If zsh isn't installed, get the platform of the current machine
    platform=$(uname);
    # If the platform is Linux, try an apt-get to install zsh and then recurse
    if [[ $platform == 'Linux' ]]; then
      if [[ -f /etc/redhat-release ]]; then
        sudo yum install zsh
      fi
      if [[ -f /etc/debian_version ]]; then
        sudo apt-get install zsh
      fi
    # If the platform is OS X, tell the user to install zsh :)
    elif [[ $platform == 'Darwin' ]]; then
      echo "We'll install zsh, then re-run this script!"
      brew install zsh
    fi

    install_zsh
  fi
}

install_zsh


###############################################################################
# Other tools                                                                 #
###############################################################################

# Temporary folder
mkdir tmp

# Create a Sites directory
# This is a default directory for macOS user accounts but doesn't comes pre-installed
# if [ ! -d "${HOME}/Sites" ]; then
#   mkdir $HOME/Sites
# fi

# Add online check for terminal
chmod +x utils/online.sh
crontab -l > $HOME/cronfile
echo "* * * * * ~/.dotfiles/utils/online.sh" >> $HOME/cronfile
crontab $HOME/cronfile
rm $HOME/cronfile

# Powerline fonts
git clone https://github.com/powerline/fonts.git tmp/fonts
./tmp/fonts/install.sh

# Temporary folder
rm -rf tmp


###############################################################################
# Symlink dotfiles                                                            #
###############################################################################

source symlinks.sh


###############################################################################
# Finish                                                                      #
###############################################################################

echo "Some changes will not take effect until you reboot your machine."

# See if the user wants to reboot.
reboot_or || source ~/.zshrc
