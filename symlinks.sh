#!/usr/bin/env bash

source utils/utils.sh

# Zsh
symlink_file $HOME/.dotfiles/shell/.zshrc $HOME/.zshrc

# Git
symlink_file $HOME/.dotfiles/git/.gitconfig $HOME/.gitconfig
symlink_file $HOME/.dotfiles/git/.gitignore_global $HOME/.gitignore_global

# Vim
symlink_file $HOME/.dotfiles/vim/.vimrc $HOME/.vimrc

# Setup bin
symlink_file $HOME/.dotfiles/bin $HOME/bin
chmod +x $HOME/bin/*
